﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using FlashcardsLibrary;
using System.Collections.Generic;

namespace GestureCardFlip
{
    [Activity(Label = "GestureCardFlip", MainLauncher = true, Icon = "@drawable/icon", Theme = "@style/CustomActionBarTheme")]
    public class MainActivity : Activity
    {
        private bool mShowingBack;
        public GestureDetector mGestureDetector;

        string russianWord = "программирование";
        string polishWord = "programowanie";

        public event EventHandler<CardFrontEventArgs> CardFrontEventHandler;
        LinearLayout nextLinearLayout;

        protected async  override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();
           SQLitePCL.Batteries.Init();

            var azureService = new AzureServices();

            //var questions = await azureService.GetQuestions();
            List<Questions> test = new List<Questions>();
            test.Add(new Questions { PolishWord = "Chyba", RussianWord = "Наверное" });
            await azureService.InsertQuestion(test);
          // Console.WriteLine(questions.Count.ToString());
            ActionBar.SetCustomView(Resource.Layout.action_bar);
            ActionBar.SetDisplayShowCustomEnabled(true);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            mGestureDetector = new GestureDetector(this, new MyGestureListener(this));

            nextLinearLayout = FindViewById<LinearLayout>(Resource.Id.nextLinearLayout);
            nextLinearLayout.Click += NextLinearLayout_Click;   

            if (bundle == null)
            {
                FragmentTransaction transaction = FragmentManager.BeginTransaction();
                transaction.Add(Resource.Id.container, new CardFrontFragment(this, polishWord));
                transaction.Commit();
            }
        }

        private void NextLinearLayout_Click(object sender, EventArgs e)
        {
            russianWord = "компьютер";
            polishWord = "komputer";
            CardFrontEventHandler.Invoke(this, new CardFrontEventArgs(polishWord, russianWord));
        }

        public void FlipCard()
        {
            if (mShowingBack)
            {
                FragmentTransaction transaction = FragmentManager.BeginTransaction();
                transaction.SetCustomAnimations(Resource.Animation.card_flip_left_in, Resource.Animation.card_flip_left_out,
                                              Resource.Animation.card_flip_right_in, Resource.Animation.card_flip_right_out);

                transaction.Replace(Resource.Id.container, new CardFrontFragment(this, polishWord));

                transaction.AddToBackStack(null);

                transaction.Commit();
                mShowingBack = false;
            }

            else
            {
                //Otherwise the front is showing therefore flip to back
                FragmentTransaction transaction = FragmentManager.BeginTransaction();

                //Set the custom animations we made to animate with this transaction
                transaction.SetCustomAnimations(Resource.Animation.card_flip_right_in, Resource.Animation.card_flip_right_out,
                                                Resource.Animation.card_flip_left_in, Resource.Animation.card_flip_left_out);

                transaction.Replace(Resource.Id.container, new CardBackFragment(this, russianWord));

                transaction.AddToBackStack(null);

                transaction.Commit();

                mShowingBack = true;
            }
        }
    }

    public class CardFrontFragment : Fragment
    {

        TextView polishWord;
        private MainActivity mMainActivity;
        string polishword;
        public CardFrontFragment(MainActivity mA, string polishWordd)
        {
            mMainActivity = mA;
            polishword = polishWordd;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            mMainActivity.CardFrontEventHandler += MMainActivity_CardFrontEventHandler;
            View frontCard = inflater.Inflate(Resource.Layout.card_front, container, false);
            frontCard.Touch += frontCard_Touch;
            polishWord = frontCard.FindViewById<TextView>(Resource.Id.polishWord);
            polishWord.Text = polishword;

            return frontCard;
        }

        private void MMainActivity_CardFrontEventHandler(object sender, CardFrontEventArgs e)
        {
            polishWord.Text = e.PolishWord;
        }
        void frontCard_Touch(object sender, View.TouchEventArgs e)
        {
            MainActivity parentActivity = Activity as MainActivity;
            parentActivity.mGestureDetector.OnTouchEvent(e.Event);
        }
    }

    public class CardBackFragment : Fragment
    {

        private TextView russianWord;
        private MainActivity mMainActivity;
        string russianword;
        public CardBackFragment(MainActivity mA, string russianWordd)
        {
            mMainActivity = mA;
            russianword = russianWordd;

        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View backCard = inflater.Inflate(Resource.Layout.card_back, container, false);
            backCard.Touch += backCard_Touch;
            russianWord = backCard.FindViewById<TextView>(Resource.Id.russianWordTextView);
            mMainActivity.CardFrontEventHandler += MMainActivity_CardFrontEventHandler;
            russianWord.Text = russianword;
            return backCard;
        }

        private void MMainActivity_CardFrontEventHandler(object sender, CardFrontEventArgs e)
        {
            russianWord.Text = e.RussianWord;
        }

        void backCard_Touch(object sender, View.TouchEventArgs e)
        {
            MainActivity parentActivity = Activity as MainActivity;
            parentActivity.mGestureDetector.OnTouchEvent(e.Event);
        }
    }


    public class CardFrontEventArgs : EventArgs
    {
        private string polishWord;
        private string russianWord;

        public string PolishWord
        {
            get { return polishWord; }
            set { polishWord = value; }
        }

        public string RussianWord
        {
            get { return russianWord; }
            set { russianWord = value; }
        }



        public CardFrontEventArgs(string polishWord, string rusianWord) : base()
        {
            PolishWord = polishWord;
            RussianWord = rusianWord;
        }
    }









    public class MyGestureListener : GestureDetector.SimpleOnGestureListener
    {
        private MainActivity mMainActivity;

        public MyGestureListener (MainActivity activity)
        {
            mMainActivity = activity;
        }

        public override bool OnDoubleTap(MotionEvent e)
        {
            mMainActivity.FlipCard();
            return true;
        }

        public override void OnLongPress(MotionEvent e)
        {
            Console.WriteLine("Long Press");
        }

        public override bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
        {
            Console.WriteLine("Fling");
            return base.OnFling(e1, e2, velocityX, velocityY);
        }

        public override bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
        {
            Console.WriteLine("Scroll");
            return base.OnScroll(e1, e2, distanceX, distanceY);
        }
    }
}

