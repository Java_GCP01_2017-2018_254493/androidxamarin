﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace GestureCardFlip
{
    class Questions
    {

        public string Id { get; set; }

        public string PolishWord { get; set; }

        public string RussianWord { get; set; }

    }
}