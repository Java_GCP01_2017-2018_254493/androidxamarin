﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.Sync;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using System.Runtime.CompilerServices;
using System.IO;

//[assembly: Dependency (typeof(AzureService))]
namespace GestureCardFlip
{
    class AzureServices
    {
        MobileServiceClient client { get; set; }

        IMobileServiceSyncTable<Questions> table;

        public async Task Initialize()
        {
            if (client?.SyncContext?.IsInitialized ?? false)
                return;

            var azureUrl = "http://flashcardsdemo.azurewebsites.net";

            //Creating client
            client = new MobileServiceClient(azureUrl);
          
            //Initialization LocalDatabase for path
            var path = "questions.db";
            path = Path.Combine(MobileServiceClient.DefaultDatabasePath, path);

            //setup our local sqlite store and intialize our table
            var store = new MobileServiceSQLiteStore(path);

            //Define table 
            store.DefineTable<Questions>();

            //Intalize SyncContext
            await client.SyncContext.InitializeAsync(store, new MobileServiceSyncHandler());

            table = client.GetSyncTable<Questions>();

        }


        public async Task<List<Questions>> GetQuestions()
        {
            await Initialize();
            await SyncQuestion();
            return await table.ToListAsync();
        }

        private async Task SyncQuestion()
        {
            try
            {
                await client.SyncContext.PushAsync();
                await table.PullAsync("allQuestions", table.CreateQuery());

            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Unable to sync, using offline capabilites: " + ex);
            }
        }

        public async Task InsertQuestion(List<Questions> questions)
        {
            await Initialize();

            await Task.WhenAll(questions.Select(q => table.InsertAsync(q)));

            await SyncQuestion();
    }
    }

    
}